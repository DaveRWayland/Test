import React, { useEffect, useState, useContext } from "react";
import Product from "./Product";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import AppContext from "../context/AppContext";

function Products() {
  const { state, addToCart } = useContext(AppContext);
  const { items } = state;

  const handleAddToCart = (item) => () => {
    addToCart(item);
  };
  return (
    <>
      <Row>
        {items.map((item) => (
          <Col md={4} key={item.id}>
            <div className="d-flex justify-content-center m-3">
              <Product
                key={item.id}
                product={item}
                handleAddToCart={handleAddToCart}
              />
            </div>
          </Col>
        ))}
      </Row>
    </>
  );
}

export default Products;
