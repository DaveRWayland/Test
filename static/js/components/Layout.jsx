import React from "react";
import Navbar from "./Navbar";
import Container from "react-bootstrap/Container";

const Layout = ({ children }) => {
  return (
    <>
      <Navbar />
      <Container>{children}</Container>
      <footer></footer>
    </>
  );
};

export default Layout;
