import React, { useContext } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Logo from "../../images/ecomsur-logo.png";
import { Link } from "react-router-dom";
import AppContext from "../context/AppContext";

const NavBar = () => {
  const { state } = useContext(AppContext);
  const { cart } = state;
  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Link to="/">
          <img
            src={Logo}
            width="30"
            height="30"
            className="d-inline-block align-top mr-2"
            alt="React Bootstrap logo"
          />
        </Link>
        <Navbar.Brand>Ecomsur</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link>
              <Link to="/cart">
                <i className="fas fa-shopping-basket"></i>
              </Link>
            </Nav.Link>
            <Nav.Link>{cart.length > 0 && <div>{cart.length}</div>}</Nav.Link>
          </Nav>
          <Nav>
            <Form inline>
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
              />
            </Form>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default NavBar;
