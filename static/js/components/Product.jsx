import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";

function Product({ product, handleAddToCart }) {
  return (
    <>
      <Card id="Cart" style={{ width: "18rem" }}>
        <Card.Img variant="top" src={product.imageURL} />
        <Card.Body>
          <Card.Title>{product.name}</Card.Title>
          <Card.Text>
            {product.currency} {product.price}
          </Card.Text>
          <Button onClick={handleAddToCart(product)} variant="primary">
            Add to Cart!
          </Button>
        </Card.Body>
      </Card>
    </>
  );
}

export default Product;
